# Copyright (C) 2019 Benjamin Zwick
#
# This file is part of Meshbabbler.
#
# Meshbabbler is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Meshbabbler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Meshbabbler.  If not, see <https://www.gnu.org/licenses/>.

from collections import OrderedDict
import numpy as np

from meshbabbler import config
from meshbabbler.textfile import TextFile
from meshbabbler.util import check_repeated_items


"""Number of nodes per element."""
num_nodes_per_element = {
    'C3D4': 4,
    'C3D10H': 10,
    'S3': 3,
}


class Mesh(object):
    def __init__(self, filename=None):

        self.coordinates = []
        """Coordinates of nodes (vertices)."""

        self.cells = []
        """Cell (element) connectivity."""

        self.node_to_coordinate_map = OrderedDict()
        """Node index to coordinate map."""

        self.element_to_cell_map = OrderedDict()
        """Element index to cell map."""

        self.element_types = []
        """Element types."""

        self.nsets = {}
        """Node sets."""

        self.elsets = {}
        """Element sets."""

        # TODO:
        # self.surfaces = {}
        # self.sections = {}

        if filename is not None:
            self.load(filename)

    @property
    def num_vertices(self):
        if isinstance(self.coordinates, np.ndarray):
            return self.coordinates.shape[0]
        elif isinstance(self.coordinates, list):
            return len(self.coordinates)

    @property
    def num_cells(self):
        return len(self.cells)

    @property
    def num_cell_domains(self):
        return len(self.cell_domains)

    @property
    def num_facet_domains(self):
        return len(self.facet_domains)

    def index_from_zero(self):
        """
        Reindex nodes, elements, nsets, elsets, surfaces
        using zero-based indexing.
        """
        raise NotImplementedError

    def check_integrity(self):
        """Check mesh integrity including nodes, elements,
        node sets and element sets.

        Note: Abaqus allows nodes/elements to belong to more than one set.
        This may cause conflicts with other software that uses domain markers
        defined over the entire mesh.
        """
        # Check every node has a coordinate
        if len(self.node_to_coordinate_map) != len(self.coordinates):
            print('WARNING: num_nodes != num_coordinates')

        # Check for repeated elements
        if len(self.element_to_cell_map) != len(self.cells):
            print('WARNING: num_elements != num_cells')

        # Check for repeated nodes in nsets
        for name, nodes in self.nsets.items():
            repeated = check_repeated_items(nodes)
            if len(repeated) > 0:
                print('WARNING: repeated nodes in NSET={}'.format(name))

        # Check for repeated elements in elsets
        for name, elements in self.elsets.items():
            repeated = check_repeated_items(elements)
            if len(repeated) > 0:
                print('WARNING: repeated nodes in ELSET={}'.format(name))

    def load(self, filename):
        """Load Abaqus mesh from input file named `filename`.
        """

        with TextFile(filename) as f:
            f.readline()
            while not f.eof:
                if f.line.startswith('**'):
                    self._parse_comment(f)
                    f.readline()
                    continue
                elif f.line.startswith('*'):
                    keyword = f.line_as_csv()[0].upper()
                    self._parse_option_block(f, keyword)
                    continue
                else:
                    what = f.line_as_csv()[0]
                    msg = 'expected keyword/comment but got: {}'.format(what)
                    raise RuntimeError(msg)

        # Convert lists to arrays
        self.coordinates = np.array(self.coordinates, dtype=float)

        self.check_integrity()

    def _parse_option_block(self, f, keyword):

        # Select parser for this keyword
        parsers = {
            '*ELEMENT': self._parse_element,
            '*ELSET': self._parse_elset,
            '*HEADING': self._parse_heading,
            '*NODE': self._parse_node,
            '*NSET': self._parse_nset,
            '*PREPRINT': self._parse_preprint,
            '*SYSTEM': self._parse_system,
        }
        try:
            parse = parsers[keyword]
        except KeyError:
            raise NotImplementedError('unknown keyword {}'.format(keyword))

        # Read parameters for this keyword
        # (comma placed at EOL indicates that next line is a continuation line)
        parameters = ''
        continuation = True
        while continuation:
            line = f.line.strip()
            continuation = line.endswith(',')
            parameters += line
            f.readline()
        # Discard the keyword, and split the rest into dictionary
        parameters = parameters.rstrip(',').split(',')[1:]
        parameters = [p.strip() for p in parameters]
        if len(parameters) > 0:
            try:
                generate = [p.upper() for p in parameters].index('GENERATE')
            except ValueError:
                generate = None
            if generate is not None:
                parameters.pop(generate)
            parameters = [p.strip().split('=') for p in parameters]
            # Check for repeated parameters
            pset = set()
            for p, v in parameters:
                if p.upper() not in pset:
                    pset.add(p.upper())
                else:
                    msg = "repeated parameter '{}'".format(p)
                    msg += " for keyword '{}':\n".format(keyword)
                    for p, v in parameters:
                        msg += p + '=' + v + ', '
                    raise ValueError(msg)
            parameters = [[p.upper(), v] for p, v in parameters]
            parameters = dict(parameters)
            if generate is not None:
                parameters['GENERATE'] = True
        else:
            parameters = {}

        # Read data for this keyword
        data = []
        while not f.eof:
            if f.line.startswith('**'):
                self._parse_comment(f.line)
            elif f.line.startswith('*'):
                break
            else:
                data.extend(f.line_as_csv())
            f.readline()

        parse(data, parameters)

        return

    def _parse_comment(self, line):
        if config.VERBOSE > 0:
            print(line, end='')

    def _parse_heading(self, data, parameters):
        if config.VERBOSE > 0:
            pass

    def _parse_preprint(self, data, parameters):
        if config.VERBOSE > 0:
            pass

    def _parse_system(self, data, system):
        if config.VERBOSE > 0:
            pass

    def _parse_element(self, data, parameters):

        eltype = parameters.get('TYPE')
        length = num_nodes_per_element[eltype] + 1

        if len(data) % length != 0:
            msg = 'Element data not of shape N x (num_nodes_per_element + 1) '
            msg += '(length = {})'.format(len(data))
            raise ValueError(msg)

        # elements
        for n in range(0, len(data), length):
            i = int(data[n])
            cell = [int(x) for x in data[n+1:n+length]]
            if i not in self.element_to_cell_map.keys():
                self.element_to_cell_map[i] = len(self.cells)
            else:
                raise ValueError('element {} repeated'.format(i))
            self.element_types.append(eltype)
            self.cells.append(cell)

        # element set
        self._parse_elset(data[::length], parameters)

    def _parse_elset(self, data, parameters):

        elset = parameters.get('ELSET', None)
        if elset is None:
            return
        else:
            if elset not in self.elsets:
                self.elsets[elset] = []
            if parameters.get('GENERATE', False):
                start, stop, step = [int(i) for i in data]
                self.elsets[elset].extend([i for i in range(start, stop, step)])
            else:
                self.elsets[elset].extend([int(i) for i in data])

    def _parse_node(self, data, parameters):

        # Format for each node is 'i, x1, x2, x3', hence data is N x 4; check:
        if len(data) % 4 != 0:
            raise ValueError('Node data not of shape N x 4 '
                             '(length = {})'.format(len(data)))

        # nodes and coordinates
        for n in range(0, len(data), 4):
            i = int(data[n])
            x = [float(x) for x in data[n+1:n+4]]
            if i not in self.node_to_coordinate_map.keys():
                self.node_to_coordinate_map[i] = len(self.coordinates)
            else:
                raise ValueError('node {} repeated'.format(i))
            self.coordinates.append(x)

        # node set
        self._parse_nset(data[::4], parameters)

    def _parse_nset(self, data, parameters):

        nset = parameters.get('NSET', None)
        if nset is None:
            return
        else:
            if nset not in self.nsets:
                self.nsets[nset] = []
            if parameters.get('GENERATE', False):
                start, stop, step = [int(i) for i in data]
                self.nsets[nset].extend([i for i in range(start, stop, step)])
            else:
                self.nsets[nset].extend([int(i) for i in data])
