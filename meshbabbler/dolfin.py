# Copyright (C) 2019 Benjamin Zwick
#
# This file is part of Meshbabbler.
#
# Meshbabbler is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Meshbabbler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Meshbabbler.  If not, see <https://www.gnu.org/licenses/>.

import dolfin


def load(filename: str) -> dolfin.Mesh:
    # TODO: work in progress
    raise NotImplementedError

    print("Reading DOLFIN mesh '{}' ".format(filename), end='')

    if filename.endswith('.hdf5'):
        fileformat = "HDF5"
    # File class compatible file formats
    elif filename.endswith('.xml'):
        fileformat = "XML"
    elif filename.endswith('.xml.gz'):
        fileformat = "compressed XML"
    else:
        ext = filename.partition('.')[2]
        if len(ext) > 0:
            print()
            raise RuntimeError(
                "Output file extension '.{}' not recognised".format(ext))
        else:
            print()
            raise RuntimeError(
                "Output file has no extension (cannot detect file format)")

    print("({} file format)... ".format(fileformat), end='')

    # File class compatible file formats
    dolfin_file_formats = ("XML",
                           "compressed XML")

    if fileformat in dolfin_file_formats:
        mesh = dolfin.Mesh(filename)
    elif fileformat in ('HDF5'):
        with dolfin.HDF5File(dolfin.MPI.comm_world, filename, 'r') as f:
            mesh = dolfin.Mesh()
            f.read(mesh, "mesh", False)
            # Create mesh functions and read sub domain data from file
            dim = mesh.topology().dim()
            cell_domains = dolfin.MeshFunction("size_t", mesh, dim)
            f.read(cell_domains, "mesh/cell_domains")
            facet_domains = dolfin.MeshFunction("size_t", mesh, dim - 1)
            f.read(facet_domains, "mesh/facet_domains")
        # TODO: set mesh domain markers
    else:
        print()
        raise NotImplementedError("fileformat == '{}'".format(fileformat))

    print("done")


def save(filename: str, mesh: dolfin.Mesh) -> None:
    """Write mesh to a DOLFIN compatible file format
    """

    print("Writing DOLFIN mesh '{}' ".format(filename), end='')

    if filename.endswith('.hdf5'):
        fileformat = "HDF5"
    elif filename.endswith('.xdmf'):
        # TODO: fileformat = "XDMF"
        raise NotImplementedError
    # File class compatible file formats
    elif filename.endswith('.bin'):
        fileformat = "Binary"
    elif filename.endswith('.raw'):
        fileformat = "RAW"
    elif filename.endswith('.svg'):
        fileformat = "SVG"
    elif filename.endswith('.xd3'):
        fileformat = "XD3"
    elif filename.endswith('.xml'):
        fileformat = "XML"
    elif filename.endswith('.xml.gz'):
        fileformat = "compressed XML"
    elif filename.endswith('.xyz'):
        fileformat = "XYZ"
    elif filename.endswith('.pvd'):
        fileformat = "VTK"
    else:
        ext = filename.partition('.')[2]
        if len(ext) > 0:
            print()
            raise RuntimeError(
                "Output file extension '.{}' not recognised".format(ext))
        else:
            print()
            raise RuntimeError(
                "Output file has no extension (cannot detect file format)")

    print("({} file format)... ".format(fileformat), end='')

    dim = mesh.topology().dim()

    # File class compatible file formats
    dolfin_file_formats = ("Binary",
                           "RAW",
                           "SVG",
                           "XD3",
                           "XML",
                           "compressed XML",
                           "XYZ",
                           "VTK")

    if fileformat in dolfin_file_formats:
        file = dolfin.File(filename)
        file << mesh
    elif fileformat in ('HDF5'):
        with dolfin.HDF5File(dolfin.MPI.comm_world, filename, 'w') as f:
            f.write(mesh, 'mesh')
            f.write(dolfin.MeshFunction(
                "size_t", mesh, dim, mesh.domains()), 'mesh/cell_domains')
            f.write(dolfin.MeshFunction(
                "size_t", mesh, dim - 1, mesh.domains()), 'mesh/facet_domains')
            f.close()
    else:
        print()
        raise NotImplementedError("fileformat == '{}'".format(fileformat))

    print("done")


def mesh(coordinates, cells,
         celltype=None,
         tdim=None,
         gdim=None,
         degree=1):
    """
    Create a DOLFIN mesh from coordinates and cell connectivity.

    Args:
        coordinates: Array of vertex coordinates.
        cells: Array of cell connectivity.
        celltype (str): Mesh cell type.
        tdim (int): The topological dimension.
        gdim (int): The geometrical dimension.
        degree (int): The polynomial degree.

    Returns:
        mesh (dolfin.Mesh):
    """

    print("Creating DOLFIN mesh:")

    # celltypes[topological dimension][number of vertices per cell]
    celltypes = {
        0: {
            1: 'point',
        },
        1: {
            2: 'interval',
        },
        2: {
            3: 'triangle',
            4: 'quadrilateral',
        },
        3: {
            4: 'tetrahedron',
            8: 'hexahedron',
        },
    }

    if gdim is None:
        gdim = coordinates.shape[1]

    if tdim is None:
        tdim = gdim

    num_vertices_per_cell = cells.shape[1]

    try:
        if celltype is None:
            celltype = celltypes[tdim][num_vertices_per_cell]
        elif celltype != celltypes[tdim][num_vertices_per_cell]:
            raise ValueError
    except Exception as exc:
        if celltype is None:
            msg = '('
        else:
            msg = '(celltype: {}, '.format(celltype)
        msg += 'vertices per cell: {}, '.format(num_vertices_per_cell)
        msg += 'gdim: {}, '.format(gdim)
        msg += 'tdim: {})'.format(tdim)
        raise ValueError('invalid cells ' + msg) from exc

    num_vertices = coordinates.shape[0]
    num_cells = cells.shape[0]

    mesh = dolfin.Mesh()

    me = dolfin.MeshEditor()
    me.open(mesh, celltype, tdim, gdim, degree=degree)

    me.init_vertices(num_vertices)
    for i, x in enumerate(coordinates):
        me.add_vertex(i, x)

    me.init_cells(num_cells)
    for i, v in enumerate(cells):
        me.add_cell(i, v)

    mesh.order()
    mesh.init()

    print("  Added {} vertices".format(mesh.num_vertices()))
    print("  Added {} cells".format(mesh.num_cells()))

    return mesh
