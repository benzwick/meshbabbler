# Copyright (C) 2019 Benjamin Zwick
#
# This file is part of Meshbabbler.
#
# Meshbabbler is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Meshbabbler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Meshbabbler.  If not, see <https://www.gnu.org/licenses/>.


def check_num_items(expected, what, found):
    msg = "Warning: Expected {} {} but found {} so far."
    if expected != found:
        print(msg.format(expected, what, found))


def check_repeated_items(items):
    """Check if any items are repeated and return them as a set."""

    unique = set()
    repeated = set()

    for i in items:
        if i in unique:
            repeated.add(i)
        else:
            unique.add(i)

    return repeated
