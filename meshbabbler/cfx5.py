# Copyright (C) 2019 Benjamin Zwick
#
# This file is part of Meshbabbler.
#
# Meshbabbler is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Meshbabbler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Meshbabbler.  If not, see <https://www.gnu.org/licenses/>.

from collections import OrderedDict
import numpy as np
import dolfin
from meshbabbler.textfile import TextFile
from meshbabbler.util import check_num_items
import meshbabbler.dolfin


comm = dolfin.MPI.comm_world

if comm.size > 1:
    raise NotImplementedError("Can only read .cfx5 files in serial")


class Mesh(object):
    def __init__(self, filename=None):

        self._index_origin = 1

        if filename is not None:
            data = load(filename)

            self.coordinates = data['coordinates']
            """Coordinates of all vertices."""

            self.cells = data['cells']
            """Cell connectivity."""

            self.cell_domains = data['cell_domains']
            """Dictionary of cell domains.
            Each cell domain is an array of cell indices."""

            self.facet_domains = data['facet_domains']
            """Dictionary of facet domains.
            Each facet domain is an array of cell and local facet indices.
            """

    @property
    def num_vertices(self):
        return self.coordinates.shape[0]

    @property
    def num_cells(self):
        return self.cells.shape[0]

    @property
    def num_cell_domains(self):
        return len(self.cell_domains)

    @property
    def num_facet_domains(self):
        return len(self.facet_domains)

    def index_one_to_zero(self):
        """Convert index numbering from one-based (index origin = 1)
        to zero-based (index origin = 0)
        """

        if self._index_origin == 1:
            assert self.cells.min() >= 1
            self.cells -= 1

            for indices in self.cell_domains.values():
                assert indices.min() >= 1
                indices -= 1

            for indices in self.facet_domains.values():
                assert indices.min() >= 1
                indices -= 1

            self._index_origin = 0
            return True
        else:
            return False

    def index_zero_to_one(self):
        """Convert index numbering from zero-based (index origin = 0)
        to one-based (index origin = 1)
        """

        if self._index_origin == 0:
            assert self.cells.min() >= 0
            self.cells += 1

            for indices in self.cell_domains.values():
                assert indices.min() >= 0
                indices += 1

            for indices in self.facet_domains.values():
                assert indices.min() >= 0
                indices += 1

            self._index_origin = 1
            return True
        else:
            return False

    @property
    def index_origin(self):
        return self._index_origin

    def as_dolfin(self):
        """Create a DOLFIN mesh from CFX5 data
        """

        if self.index_origin == 1:
            self.index_one_to_zero()
            reset_index_origin = True

        mesh = meshbabbler.dolfin.mesh(self.coordinates, self.cells)

        mark_dolfin_cells(mesh, self.cell_domains)
        mark_dolfin_facets(mesh, self.facet_domains, self.cells)

        if reset_index_origin:
            self.index_zero_to_one()

        return mesh


def load(filename):
    """Read input from cfx5 mesh file

    Args:
        filename (str): Input file name of cfx5 file

    Returns:
        coordinates, cells, cell_domains, facet_domains
    """

    with TextFile(filename) as f:

        print("Reading cfx5 mesh file '{}'".format(filename))

        # Read and discard these header lines
        f.readline()
        f.readline()

        # Header
        l = [int(x) for x in f.readline().split()]
        num_vertices = l[0]
        num_cells = l[1]
        # What are l[2], l[3], l[4] ?
        num_cell_domains = l[5]
        num_facet_domains = l[6]

        # Coordinates
        print('Reading vertex coordinates ({} vertices)'.format(num_vertices))
        coordinates = np.empty([num_vertices, 3])
        try:
            for i in range(num_vertices):
                line = f.readline()
                coordinates[i] = np.array([float(x) for x in line.split()])
        except:
            check_num_items(num_vertices, "vertices", i)
            raise

        # Cells
        # Note: only tetrahedral cells
        print('Reading cell connectivity ({} cells)'.format(num_cells))
        cells = np.empty([num_cells, 4], dtype=int)
        try:
            for i in range(num_cells):
                line = f.readline()
                cells[i] = np.array([int(x) for x in line.split()])
        except:
            check_num_items(num_cells, "cells", i)
            raise

        # Cell domains
        print("Reading cell domains:")
        cell_domains = OrderedDict()
        for dummy in range(num_cell_domains):
            length, name = f.readline().split()
            length = int(length)
            print("  '{0}' ({1} cells)".format(name, length))
            indices = []
            try:
                while len(indices) < length:
                    line = f.readline()
                    indices += [int(x) for x in line.split()]
                    if not line:
                        break
            except:
                check_num_items(length, "cell indices", len(indices))
                raise
            check_num_items(length, "cell indices", len(indices))
            cell_domains[name] = np.array(indices)

        # Facet domains
        print("Reading facet domains:")
        facet_domains = OrderedDict()
        for dummy in range(num_facet_domains):
            length, name = f.readline().split()
            length = int(length)
            print("  '{0}' ({1} facets)".format(name, length))
            indices = []
            try:
                while len(indices) < 2 * length:
                    line = f.readline()
                    indices += [int(x) for x in line.split()]
                    if not line:
                        break
            except:
                check_num_items(2 * length, "facet indices", len(indices))
                raise
            check_num_items(2 * length, "facet indices", len(indices))
            facet_domains[name] = np.reshape(np.array(indices), [length, 2])

        f.warn_unread_lines()

    data = {'coordinates': coordinates,
            'cells': cells,
            'cell_domains': cell_domains,
            'facet_domains': facet_domains}

    return data


def save(filename, mesh):
    raise NotImplementedError


def mark_dolfin_cells(mesh, cell_domains):
    """Mark cell domains of a DOLFIN mesh
    """

    print("Marking cell domains:")

    # Mark with zero as default
    marker = 0
    for index, cell in enumerate(mesh.cells()):
        mesh.domains().set_marker((index, marker), 3)

    # Mark cell domains
    for name, cell_domain in cell_domains.items():
        marker += 1
        length = 0
        cell_set = set(cell_domain)
        for index, cell in enumerate(mesh.cells()):
            if index in cell_set:
                mesh.domains().set_marker((index, marker), 3)
                length += 1
        print("  '{0}': {1} ({2} cells)".format(name, marker, length))


def mark_dolfin_facets(mesh, facet_domains, cells):
    """Mark facet domains of a DOLFIN mesh
    """

    print("Marking facet domains:")

    # Mark with zero as default
    marker = 0
    for facet in dolfin.facets(mesh):
        mesh.domains().set_marker((facet.index(), marker), 2)

    # Note: if cfx ordering is same as dolfin this is not needed;
    # but this works in more general cases where the ordering is
    # different - may be useful for other file formats as well
    cfx_cell_facet = {0: [0, 1, 2], 1: [0, 1, 3], 2: [1, 2, 3], 3: [0, 2, 3]}

    for name, facet_domain in facet_domains.items():
        marker += 1
        facet_domain_vertices = set()

        # Get all vertices belonging to facet domain
        for facet in facet_domain:
            ci = facet[0]
            fi = facet[1]
            v = cells[ci]
            # Add vertices v of facet with local index fi w.r.t. parent cell ci
            facet_domain_vertices.update(v[cfx_cell_facet[fi]])

        # Mark facets with vertices belonging to facet domain
        length = 0
        for facet in dolfin.facets(mesh):
            facet_vertices = set(facet.entities(0).flatten())
            if facet_vertices.issubset(facet_domain_vertices):
                mesh.domains().set_marker((facet.index(), marker), 2)
                length += 1
        print("  '{0}': {1} ({2} facets)".format(name, marker, length))
