# Copyright (C) 2019 Benjamin Zwick
#
# This file is part of Meshbabbler.
#
# Meshbabbler is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Meshbabbler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Meshbabbler.  If not, see <https://www.gnu.org/licenses/>.


class TextFile(object):
    def __init__(self, filename, **kwargs):
        self._context = []
        self._eof = False
        self._file = open(filename)
        self._line = None
        self._lineno = 0

        #: Number of lines of leading context before current line.
        self.num_context_before = kwargs.get('num_context_before', 5)

    def close(self):
        return self._file.close()

    def readline(self):
        if self.eof:
            raise RuntimeError("attempt to read beyond EOF at "
                               "line {}.".format(self.lineno))

        line = self._file.readline()

        if line == str():  # empty string means EOF reached
            self._eof = True
        else:
            self._lineno += 1
            self._line = line

            # Update context
            if len(self._context) >= self.num_context_before:
                self._context.pop(0)
            if self.num_context_before > 0:
                self._context.append(line)

        return line

    def readlines(self):
        lines = self._file.readlines()

        if len(lines) > 0:
            self._line = lines[-1]
            self._lineno = self._lineno + len(lines)

        # Update context
        if self.num_context_before > 0:
            n = min(len(lines), self.num_context_before)
            self._context = lines[len(lines)-n:]

        return lines

    def line_as_csv(self):
        """Current line as comma separated values with no line ending."""
        return [x.strip() for x in self.line.strip().rstrip(',').split(',')]

    def error_msg(self, exc_value):
        print("Error: {}".format(str(exc_value)))
        print("       while reading '{}' near line {}:".format(
            self.name, self.lineno))
        print(self.context, end='')

    def warn_unread_lines(self):
        last_lineno = self.lineno
        num_ignored_lines = len(self.readlines())
        if num_ignored_lines > 0:
            print("Warning: {} line(s) after line {} were ignored.".format(
                num_ignored_lines, last_lineno))

    @property
    def context(self):
        """Context surrounding the current line."""
        return ''.join(self._context)

    @property
    def eof(self):
        return self._eof

    @property
    def line(self):
        return self._line

    @property
    def lineno(self):
        return self._lineno

    @property
    def name(self):
        return self._file.name

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_value:
            self.error_msg(exc_value)
        self.close()
