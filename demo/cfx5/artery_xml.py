import artery
import dolfin

filename = "artery.xml"

# Load DOLFIN mesh from file
mesh = dolfin.Mesh(filename)

# Create mesh functions from domain data attached to mesh
dim = mesh.topology().dim()
cell_domains = dolfin.MeshFunction("size_t", mesh, dim, mesh.domains())
facet_domains = dolfin.MeshFunction("size_t", mesh, dim - 1, mesh.domains())

# Solve artery problem
artery.run(mesh, cell_domains, facet_domains, "output/" + filename)
