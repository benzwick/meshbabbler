from dolfin import (Constant, DirichletBC, File, Function,
                    FunctionSpace, MPI, TestFunction, TrialFunction,
                    dot, dx, grad, solve)


def run(mesh, cell_domains, facet_domains, output_prefix):
    V = FunctionSpace(mesh, "CG", 1)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Constant(0.0)
    a = dot(grad(u), grad(v)) * dx
    L = f * v * dx

    # Define boundary condition values
    noslip = Constant(0.0)
    inflow = Constant(2.0)
    outflow = Constant(1.0)

    # Dictionary that maps region name to domain number
    facet_domain = {'wall': 1,
                    'outlet': 2,
                    'inlet': 3}

    # Define boundary conditions
    bcs = [DirichletBC(V, noslip, facet_domains, facet_domain['wall']),
           DirichletBC(V, inflow, facet_domains, facet_domain['inlet']),
           DirichletBC(V, outflow, facet_domains, facet_domain['outlet'])]

    # Compute solution
    u = Function(V)
    u.rename('u', 'u')
    solve(a == L, u, bcs)

    # Write solution and sub domains to file
    comm = MPI.comm_world
    if comm.size == 1:
        mode = "serial"
    else:
        mode = "mpi"
    File(output_prefix + "_" + mode + "_u.pvd") << u
    File(output_prefix + "_" + mode + "_cell_domains.pvd") << cell_domains
    File(output_prefix + "_" + mode + "_facet_domains.pvd") << facet_domains
