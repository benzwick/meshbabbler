import artery
import dolfin

filename = "artery.hdf5"

with dolfin.HDF5File(dolfin.MPI.comm_world, filename, 'r') as file:

    # Create DOLFIN mesh and read mesh data from file
    mesh = dolfin.Mesh()
    file.read(mesh, "mesh", False)

    # Create mesh functions and read domain data from file
    dim = mesh.topology().dim()
    cell_domains = dolfin.MeshFunction("size_t", mesh, dim)
    file.read(cell_domains, "mesh/cell_domains")
    facet_domains = dolfin.MeshFunction("size_t", mesh, dim - 1)
    file.read(facet_domains, "mesh/facet_domains")

# Solve artery problem
artery.run(mesh, cell_domains, facet_domains, "output/" + filename)
