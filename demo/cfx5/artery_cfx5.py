import artery
import dolfin
import meshbabbler.cfx5

filename = "artery.cfx5"

# Load DOLFIN mesh from file
mesh = meshbabbler.cfx5.Mesh(filename).as_dolfin()

# Create mesh functions from domain data attached to mesh
dim = mesh.topology().dim()
cell_domains = dolfin.MeshFunction("size_t", mesh, dim, mesh.domains())
facet_domains = dolfin.MeshFunction("size_t", mesh, dim - 1, mesh.domains())

# Solve artery problem
artery.run(mesh, cell_domains, facet_domains, "output/" + filename)
