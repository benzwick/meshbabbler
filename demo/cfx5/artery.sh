# exit on all errors
set -e

# Convert mesh files

cfx5_to_dolfin artery.cfx5 artery.xml
cfx5_to_dolfin artery.cfx5 artery.hdf5

# Serial

python artery_xml.py

python artery_cfx5.py
cmp output/artery.cfx5_serial_u000000.vtu output/artery.xml_serial_u000000.vtu

python artery_hdf5.py
cmp output/artery.hdf5_serial_u000000.vtu output/artery.xml_serial_u000000.vtu

# Parallel

mpirun -n 4 python artery_xml.py

# Cannot run with MPI:
# mpirun -n 4 python artery_cfx5.py

mpirun -n 4 python artery_hdf5.py
cmp output/artery.hdf5_mpi_u_p0_000000.vtu output/artery.xml_mpi_u_p0_000000.vtu
cmp output/artery.hdf5_mpi_u_p1_000000.vtu output/artery.xml_mpi_u_p1_000000.vtu
