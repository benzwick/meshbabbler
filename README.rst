Meshbabbler
============

Meshbabbler is a tool for converting mesh files
from one format to another.

Installation
------------

Use pip to install from the source directory::

  pip install .

Usage
-----

Convert an ICEM ``.cfx5`` file to a DOLFIN ``.xml`` file::

  cfx5_to_dolfin input.cfx5 output.xml

File Formats
------------

The following file formats are currently supported:

- Abaqus (.inp)
- DOLFIN (.xml, .hdf5)
- ICEM (.cfx5)

Contributing
------------

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

License
-------

Meshbabbler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Meshbabbler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Meshbabbler.  If not, see <https://www.gnu.org/licenses/>.
