import setuptools

with open('README.rst', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='meshbabbler',
    version='0.1.0',
    author='Ben Zwick',
    author_email='benzwick@gmail.com',
    description='A mesh conversion tool',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://gitlab.com/benzwick/meshbabbler',
    license='GPL',
    packages=setuptools.find_packages(),
    scripts=['bin/cfx5_to_dolfin'],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Topic :: Scientific/Engineering :: Mathematics',
    ],
)
